name := "tictactoe"

version := "0.2"

scalaVersion := "3.0.0-M3"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.3"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.3" % "test"
