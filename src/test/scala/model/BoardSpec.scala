package model

import org.scalatest.matchers._
import org.scalatest.wordspec.AnyWordSpec

class BoardSpec extends AnyWordSpec with should.Matchers {
  "A Board is the playing field" should {
    val board = new Board()
    "contains a matrix" should {
      "have dimension of 3 x 3" in {
        board.matrix.size should be(3)
      }
      "have values from 1 to 9 as String" in {
        board.matrix(0)(0) should be("1")
        board.matrix(1)(1) should be("5")
        board.matrix(2)(2) should be("9")
      }
    }
    "allow update the matrix of a board copy with a given sign" in {
      val updatedBoard = board.updateMatrix(0, 0, "X")
      updatedBoard.matrix(0)(0) should be("X")
      board.matrix(0)(0) should be("1")
    }
    "return coordinates of a chosen field number" in {
      val coordinates = board.getCoordinates(1)
      coordinates.getOrElse('x', 'x') should be(0)
      coordinates.getOrElse('y', 'y') should be(0)
    }
  }
}
