package model

import org.scalatest.matchers._
import org.scalatest.wordspec.AnyWordSpec

class PlayerSpec extends AnyWordSpec with should.Matchers {
  "A Player" should {
    val player = Player("Player Name", "X")
    "have a name" in {
      player.name should be("Player Name")
    }
    "have a sign" in {
      player.sign should be("X")
    }
    "have a string representation equals the sign" in {
      player.toString() should be(player.sign)
    }
  }
}
