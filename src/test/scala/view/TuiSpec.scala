package view

import controller.Gameplay
import org.scalatest.matchers._
import org.scalatest.wordspec.AnyWordSpec

class TuiSpec extends AnyWordSpec with should.Matchers {
  val controller = new Gameplay
  val tui = new Tui(controller)

  "A TicTacToe Tui" should {
    "print a welcome message" in {
      tui.welcomeText() should include("WELCOME TO TIC TAC TOE")
    }
    "print a winner message" in {
      val winnerText = tui.printWinner("Player1")
      winnerText should include("WE HAVE A WINNER")
      winnerText should include("Player1")
    }
    "print a tie message" in {
      tui.printTie() should include("OH NO, IT IS A TIE")
    }
  }
}
