package controller

import model.Board
import org.scalatest.matchers._
import org.scalatest.wordspec.AnyWordSpec

import scala.util.Success

class GameplaySpec extends AnyWordSpec with should.Matchers {
  "A Gameplay" should {
    val controller = new Gameplay()

    "create a player" in {
      val player = controller.createPlayer("Player1", "X")
      player.name should be("Player1")
      player.sign should be("X")
    }
    "create new Board" in {
      val board = controller.createNewBoard()
      board.matrix shouldBe a[Vector[Vector[String]]]
    }
    "set the sign of the given player on the board at the chosen field" in {
      val updatedBoard = controller.setSign(6, controller.createNewBoard(), controller.createPlayer("Player1", "X"))
      updatedBoard.matrix(1)(2) should be("X")
    }
    "validate the chosen field" should {
      val board = controller.createNewBoard()

      "return some if input is valid" in {
        controller.isValid(1, board) should be(Some(true))
      }
      "return none if input is not valid" in {
        controller.isValid(0, board) should be(None)
        controller.isValid(10, board) should be(None)
        val updatedBoard = board.updateMatrix(1, 0, "X")
        controller.isValid(4, updatedBoard) should be(None)
      }
    }
    "return the status if game is over" should {
      val board = controller.createNewBoard()
      "return a Success with Some(true) for a winner" in {
        val winnerMatrix = Vector.fill(3, 3)("X")
        val winnerBoard = Board(winnerMatrix)
        controller.isGameOver(winnerBoard, 4) should be(Success(Some(true)))
      }
      "return a Success with Some(false) for a tie" in {
        controller.isGameOver(board, 8) should be(Success(Some(false)))
      }
      "return a Success with None if game is not over yet" in {
        controller.isGameOver(board, 2) should be(Success(None))
      }
    }
  }
}
