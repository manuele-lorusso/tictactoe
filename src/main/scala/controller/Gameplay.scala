package controller

import model.{Board, Player}

import scala.util.{Success, Try}

class Gameplay():

  def createPlayer(name: String, sign: String): Player = {
    Player(name, sign)
  }

  def createNewBoard(): Board = {
    new Board()
  }

  def setSign(fieldNumber: Int, board: Board, player: Player): Board = {
    val coordinates = board.getCoordinates(fieldNumber)
    board.updateMatrix(coordinates.getOrElse('x', 'x'), coordinates.getOrElse('y', 'y'), player.sign)
  }

  private def available(fieldNumber: Int, board: Board): Boolean = {
    val coords = for {row <- 0 to 2
                      col <- 0 to 2
                      if board.matrix(row)(col) == fieldNumber.toString
                      } yield (row, col)

    val x = coords.headOption.getOrElse(-1, -1)._1
    val y = coords.headOption.getOrElse(-1, -1)._2

    coords != Vector() && (board.matrix(x)(y) != "X" || board.matrix(x)(y) != "O")
  }

  def isValid(fieldNumber: Int, board: Board): Option[Boolean] = {
    if (available(fieldNumber, board)) Some(true) else None
  }

  def isGameOver(board: Board, moves: Int): Try[Option[Boolean]] = {
    if getWinner(board) then
      Success(Some(true))
    else if isTie(moves) then
      Success(Some(false))
    else
      Success(None)
  }

  private def getWinner(board: Board): Boolean = {
    val matrix = board.matrix
    // Horizontals and verticals
    (0 to 2).foreach(i => {
      if (matrix(i)(0) == matrix(i)(1) && matrix(i)(0) == matrix(i)(2) ||
        matrix(0)(i) == matrix(1)(i) && matrix(0)(i) == matrix(2)(i)) return true
    })

    // Diagonals
    if (matrix(0)(2) == matrix(1)(1) && matrix(0)(2) == matrix(2)(0) ||
      matrix(0)(0) == matrix(1)(1) && matrix(0)(0) == matrix(2)(2)) return true

    false
  }

  private def isTie(moves: Int): Boolean = {
    moves == 8
  }
