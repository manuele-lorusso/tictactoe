package model

case class Player(name: String, sign: String):
  override def toString: String = {
    this.sign
  }
