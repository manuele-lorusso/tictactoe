package model

case class Board(matrix: Vector[Vector[String]]):

  def this() = this(Vector.tabulate(3, 3)((row, col) => (3 * row + col + 1).toString))

  def updateMatrix(x: Int, y: Int, sign: String): Board = {
    copy(matrix.updated(x, matrix(x).updated(y, sign)))
  }

  def getCoordinates(fieldNumber: Int): Map[Char, Int] = {
    val coords = for row <- 0 to 2
                     col <- 0 to 2
                     if this.matrix(row)(col) == fieldNumber.toString
      yield (row, col)

    Map(('x', coords.headOption.getOrElse(-1, -1)._1), ('y', coords.headOption.getOrElse(-1, -1)._2))
  }
