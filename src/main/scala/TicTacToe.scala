import controller.Gameplay
import view.Tui

@main def tictactoe: Unit =
    val controller = Gameplay()
    val tui = Tui(controller)
    tui.startGame()
