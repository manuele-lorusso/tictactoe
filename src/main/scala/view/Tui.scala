package view

import controller.Gameplay
import model.{Board, Player}

import scala.annotation.tailrec
import scala.io.StdIn.{readInt, readLine}
import scala.util.{Failure, Success, Try}

class Tui(controller: Gameplay):

  def startGame(): Unit = {
    println(welcomeText())

    println("Please type in name of player 1:")
    val name1 = readLine()
    println("Please type in name of player 2:")
    val name2 = readLine()

    val player1 = controller.createPlayer(name1, "X")
    val player2 = controller.createPlayer(name2, "O")
    println(s"\nSymbol of player ${player1.name} is ${player1.sign}")
    println(s"Symbol of player ${player2.name} is ${player2.sign} \n")
    val players = Vector[Player](player1, player2)
    val r = scala.util.Random
    val startPlayerIndex = r.nextInt(2)

    val board = controller.createNewBoard()

    play(players, startPlayerIndex, board, 0) match {
      case Some(playerName) => println(printWinner(playerName))
      case None => println(printTie())
    }
  }

  @tailrec
  private def play(players: Vector[Player], playerIndex: Int, board: Board, moves: Int): Option[String] = {
    drawBoard(board)

    val player = players(playerIndex)
    println(s"Player ${player.name}, it's your turn.")
    println(s"Select your field number:")

    Try(readInt()) match {
      case Failure(_) =>
        println("Wrong input. Please type the number of the field where you would like to set your sign!")
        play(players, playerIndex, board, moves)
      case Success(fieldNumber) =>
        controller.isValid(fieldNumber, board) match {
          case Some(true) =>
            val updatedBoard = controller.setSign(fieldNumber, board, player)
            controller.isGameOver(updatedBoard, moves) match {
              case Success(value) => value match {
                case Some(true) =>
                  drawBoard(updatedBoard)
                  Some(player.name)
                case Some(false) =>
                  drawBoard(updatedBoard)
                  None
                case None =>
                  val nextPlayerIndex = if (playerIndex == 0) 1 else 0
                  play(players, nextPlayerIndex, updatedBoard, moves + 1)
              }
              case Failure(_) =>
                println("Should not happen!")
                play(players, playerIndex, board, moves)
            }
          case Some(false) =>
            println("Should not happen!")
            play(players, playerIndex, board, moves)
          case None =>
            println("Not available number. Please type a free number of the field!")
            play(players, playerIndex, board, moves)
        }
    }
  }

  def welcomeText(): String =
    """
    ==============================
        WELCOME TO TIC TAC TOE
    ==============================
  """

  def drawBoard(board: Board): Unit = {
    board.matrix.indices.foreach(i => {
      print(" ")
      print(board.matrix(i)(0))
      print(" | ")
      print(board.matrix(i)(1))
      print(" | ")
      print(board.matrix(i)(2))

      if (i < 2)
        println("\n---+---+---")
    })

    println("\n")
  }

  def printWinner(playerName: String): String =
    s"""
    ==============================
          WE HAVE A WINNER!
           CONGRATULATIONS
             $playerName
    ==============================
  """

  def printTie(): String =
    """
    ==============================
         OH NO, IT IS A TIE
    ==============================
  """
